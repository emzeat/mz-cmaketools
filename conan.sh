#!/usr/bin/env bash
# conan.sh
#
# Copyright (c) 2008 - 2024 Marius Zwicker
# All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

conan_version=1.61.0

echo "== configuring conan '$conan_version'"

build_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
venv=$build_dir/conan-venv

# macOS has md5, Linux is using md5sum
if command -v md5 > /dev/null; then
    md5=md5
else
    md5=md5sum
fi

# may not have python but only python3
if command -v python > /dev/null; then
    python_cmd=python
else
    python_cmd=python3
fi

# unix has different way to activate than windows
if [[ "$(uname -a)" == *"MSYS_NT"* ]] || [[ "$(uname -a)" == *"MINGW64_NT"* ]]; then
    activate_venv=$venv/Scripts/activate
else
    activate_venv=$venv/bin/activate
fi

md5_new=$($md5 $build_dir/conan.sh)
md5_inst=$(cat $venv/conan.sh.md5sum 2>/dev/null || true)
if [ ! "$md5_new" = "$md5_inst" ]; then
    echo "-- need to initialize python venv '${venv}'"
    if [ -f $activate_venv ]; then
        true
    else
        $python_cmd -m venv $venv
    fi
    source $activate_venv
    echo "$md5_new" > $venv/conan.sh.md5sum
else

    source $activate_venv
fi

if ! conan --version | grep -q ${conan_version}; then
    echo "-- need to upgrade Conan using $(which $python_cmd)"
    $python_cmd -m pip install \
        --disable-pip-version-check \
        conan==${conan_version}
fi

echo "-- $(which conan)"
echo "-- running $(conan --version)"
echo
conan config set general.revisions_enabled=1
conan config set general.skip_broken_symlinks_check=True
